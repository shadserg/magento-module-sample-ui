<?php
namespace Shadserg\SampleUi\Component;

class Simple extends \Magento\Ui\Component\AbstractComponent
{
    const NAME = 'shadserg_sampleui_simple';

    public function getComponentName()
    {
        return static::NAME;
    }
}