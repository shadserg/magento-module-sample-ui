define([
    'uiElement',
    'ko'
], function(Element, ko){

    console.log('init Shadserg_SampleUi/js/component/simple.js');

    return Element.extend({
        defaults: {
            template: 'Shadserg_SampleUi/shadserg_sampleui/simple'
        },
        message: ko.observable("Hello Knockout.js!")
    });
});